FROM debian


RUN true \
  && groupadd -g 911 linuxserver \
  && useradd -u 911 -g linuxserver -G cdrom linuxserver \
  && sed -i 's/main$/main contrib non-free/' /etc/apt/sources.list \
  && apt-get -y update \
  && DEBIAN_FRONTEND=noninteractive apt-get --no-install-recommends -y install \
    dvdbackup lsdvd libdvd-pkg libdvdcss2 \
    cd-discid cdparanoia lame \
    handbrake-cli libavcodec-extra \
    python3 python3-slugify \
  && true
RUN dpkg-reconfigure libdvd-pkg

COPY src/* /app/
COPY abcde.conf httpd.conf /etc/
COPY --chown=linuxserver:linuxserver www /www
USER linuxserver
ENTRYPOINT ["python3", "/app/sucker.py"]

# vi: ts=2 sw=2 et ai
