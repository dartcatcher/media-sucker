#! /usr/bin/python3

import argparse
import http.server
import pathlib
import logging

import reader, encoder, statuser

class HTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, r, a, s, directory, statuser):
        self.statuser = statuser
        self.quiet = False
        super().__init__(r, a, s, directory=directory)

    def log_request(self, code="-", size="-"):
        if not self.quiet:
            super().log_request(code, size)

    def do_GET(self):
        if self.path == "/status.json":
            return self.get_status()
        return super().do_GET()

    def get_status(self):
        self.quiet = True
        self.send_response(200)
        self.end_headers()
        buf = self.statuser.json().encode("utf-8")
        self.wfile.write(buf)

def main():
    parser = argparse.ArgumentParser(description="Rip/encode optical media")
    parser.add_argument("-incoming", type=pathlib.Path, default="/incoming")
    parser.add_argument("-www", type=pathlib.Path, default="/www")
    parser.add_argument("-port", type=int, default=8080)
    parser.add_argument("-drive", nargs="+", default=["/dev/sr0", "/dev/sr1"])
    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    readers = [reader.Reader(d, args.incoming) for d in args.drive]
    encoders = [encoder.Encoder(args.incoming) for i in range(1)]
    st = statuser.Statuser(readers + encoders, args.incoming)

    [w.start() for w in readers + encoders]
    st.start()

    handler = lambda r, a, s: HTTPRequestHandler(r, a, s, directory=args.www, statuser=st)
    httpd = http.server.ThreadingHTTPServer(('', args.port), handler)
    httpd.serve_forever()

if __name__ == "__main__":
    main()

# vi: sw=4 ts=4 et ai
