#! /usr/bin/python3

import os
import subprocess
import glob
import os
import json
import io
import shutil
import time
import re
import logging
import dvd
import cd
import worker

class Encoder(worker.Worker):
    def __init__(self, directory=None):
        self.status = {}
        self.directory = directory
        self.loop_delay = 12
        return super().__init__(directory)

    def once(self):
        wait = True
        self.status["type"] = "encoder"
        for fn in glob.glob(self.workdir("*", "sucker.json")):
            directory = os.path.dirname(fn)
            state = self.read_state(directory)
            self.encode(directory, state)

    def encode(self, directory, state):
        self.status["state"] = "encoding"
        self.status["title"] = state["title"]

        if state["video"]:
            media = dvd
        else:
            media = cd

        logging.info("Encoding %s (%s)" % (directory, state["title"]))
        for pct in media.encode(state, directory):
            self.status["complete"] = pct

        media.clean(state, directory)
        self.clear_state(directory)

        logging.info("Finished encoding")

# vi: sw=4 ts=4 et ai
