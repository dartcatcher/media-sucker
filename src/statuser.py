#! /usr/bin/python3

import threading
import json
import glob
import time
import os

class Statuser(threading.Thread):
    def __init__(self, workers, directory):
        self.workers = workers
        self.directory = directory
        self.status = {}
        super().__init__(daemon=True)

    def run(self):
        while True:
            self.status["finished"] = {
                "video": glob.glob(os.path.join(self.directory, "*.mkv")),
                "audio": glob.glob(os.path.join(self.directory, "*/*.mp3")),
            }
            self.status["workers"] = [w.status for w in self.workers]
            time.sleep(12)

    def json(self): 
        return json.dumps(self.status)

# vi: sw=4 ts=4 et ai
