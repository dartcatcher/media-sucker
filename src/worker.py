import threading 
import os
import json
import logging
import time
import traceback

SECOND = 1
MINUTE = 60 * SECOND
HOUR = 60 * MINUTE

class Worker(threading.Thread):
    def __init__(self, directory, **kwargs):
        self.directory = directory
        self.loop_delay = 2 * SECOND
        self.status = {}

        kwargs["daemon"] = True
        return super().__init__(**kwargs)

    def run(self):
        while True:
          self.status = {"state": "idle"}
          try:
              self.once()
          except Exception as exc:
              self.status["state"] = "error"
              for line in traceback.format_exception(exc):
                  logging.error(line)
              time.sleep(30 * SECOND)
          time.sleep(self.loop_delay)

    def workdir(self, *path):
        return os.path.join(self.directory, *path)

    def write_state(self, subdir, state):
        logging.debug("Writing state: %s" % repr(state))
        statefn = self.workdir(subdir, "sucker.json")
        newstatefn = statefn + ".new"
        with open(newstatefn, "w") as f:
            json.dump(state, f)
        os.rename(newstatefn, statefn)

    def read_state(self, subdir):
        try:
            with open(self.workdir(subdir, "sucker.json")) as f:
                return json.load(f)
        except FileNotFoundError:
            return {}

    def clear_state(self, subdir):
        try:
            os.unlink(self.workdir(subdir, "sucker.json"))
        except FileNotFoundError:
            pass
