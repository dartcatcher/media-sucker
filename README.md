The Media Sucker
================

This program watches your CD/DVD drive.
When you put a CD or DVD in, 
it will suck the content off,
eject the drive,
and then re-encode the content to a compressed format.

## What It Supports

At the time I'm writing this README, it will:

* Rip audio CDs, look them up in cddb, encode them to VBR MP3, then tag them. 
  * It also writes a shell script you can modify to quickly change the tags, since this is a pretty common thing to want to do.
* Rip video DVDs, transcode them to mkv


## Requirements

The requirements are fairly light: a few CD tools, cdparanoia, HandBrakeCLI, and some
DVD libraries.

Most notably, you do *not* need a relational database (SQLite, Postgres, MySQL).
You just need a file system.

For a complete list of requirements,
look at the [Dockerfile](Dockerfile) 
to see what Debian packages it installs.


## How To Run This

You need a place to store your stuff.
Mine is `/srv/incoming`.

    docker run \
        --name=sucker \
        -d --restart=always \
        -p 8080:8080 \
        --device /dev/sr0 \
        -v /srv/incoming:/incoming \
        registry.gitlab.com/dartcatcher/media-sucker/media-sucker

I can't get it to work with docker swarm,
which doesn't support `--device`.

Stick a video DVD or audio CD in,
and the drive should spin up for a while,
then spit your media back out.
Then, eventually, you'll have a new `.mkv` file (for video)
or a new directory of `.mp3` files (for audio).

You can watch what it's doing at http://localhost:8080/


## A note on filenames and tags

This program does the absolute minimum to try and tag your media properly.
Partly because I'm a lazy programmer,
but mostly because the computer can only guess at things that you,
the operator,
can just read off the box.

For DVDs, that means reading the "title" stored on the DVD,
which I've seen vary from very helpful (eg. "Barbie A Fashion Fairytale")
to ridiculously unhelpful (eg. "FBWTF2").
But at least it's usually unique for each DVD and at least somewhat
related to the DVD contents.

For CDs, the situation is even worse.
Audio CDs do not store any metadata,
so CDDB takes the length of every track in seconds and tries to match that
against something a user has uploaded in the past.
This is wrong a whole lot of the time.

But the end result in almost every case is that you're going to have to
rename the movie file, or re-tag the audio files.
This is why you get a `tag.sh` file with every audio CD rip.


## Answers

I'm skipping the part where I make up questions I think people might have.


### Audio tracks aren't tagged right

This happens a lot,
so every audio encode comes with a shell script you can edit
to re-tag everything.

### TV episodes encode as one giant file

Some DVDs with TV episodes will rip as one big track.
You can use the included `split.py` to split those into multiple files,
based on chapter marks.
If you don't give it chapter ranges,
it will try to make an intelligent guess.

### Why I Wrote This

The automatic-ripping-machine looks really badass.
But after multiple attempts across multiple months
to get it running,
I decided it would probably be faster just to write my own.

media-sucker isn't as cool as the automatic-ripping-machine.
But, at least for me,
it's more useful,
in that I can get it to actually do something.

### Why You Should Run This

The only reason I can think of that anybody would want to use this is if they,
like me,
are too dumb to get the automatic-ripping-machine to work.

### What Kind Of Hardware I Use

I run it on a Raspberry Pi 4,
with a Samsung DVD drive from the stone age.
