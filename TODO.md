To Do
-----

* Status web app as PWA? Does this even matter? Maybe not.
* The directory layout is gross. Fix it.
  * What I'd like to see: pending/ has everything that's not finished yet; complete/ has everything that is.
  * Do I really need to separate audio and video? Can't I just put that in the json file?
* I'm not certain the CD part works after moving to Python from Bourne Shell.
* Set title for audio CDs on read.
* For audio CDs, set completion to null, and have that result in a spinny progress meter.


Done
----

* Handbrake is putting the stereo track before the 5.1 track, and making stereo the default. Fix that.
* Status web app
* Report title in status.json
* Output JSON instead of bourne shell for env
  * New function in common: getenv() { jq -r --arg key="$1" '.[key]' env.json }
  * Then just slurp the thing into the status json
* Indicate what is in the encoding queue (# items, titles)
* Completion percentage scraping
  * Handbrake: `Encoding: task 2 of 2, 20.58%`
  * dvdbackup: `Copying Title, part 2/4: 72% done`
