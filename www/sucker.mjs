const MILLISECOND = 1
const SECOND = 1000

async function update() {
  let resp = await fetch("status.json", {cache: "no-store"})
  let s = await resp.json()

  let jobs = document.querySelector(".workers.jobs")

  while (jobs.firstChild) jobs.firstChild.remove()

  for (let worker of s.workers) {
    if (worker.state != "idle") {
      let job = jobs.appendChild(document.createElement("div"))

      let tag = job.appendChild(document.createElement("span"))
      tag.classList.add("tag", "is-info")
      tag.textContent = worker.type

      let txt = job.appendChild(document.createElement("span"))
      txt.textContent = worker.title
      txt.classList.add("mx-1")

      if (worker.complete) {
        let progress = job.appendChild(document.createElement("progress"))
        progress.classList.add("is-primary")
        progress.value = worker.complete
      }
    }
  }

  let fileItem = document.querySelector("template.panel-file-item").content

  for (let section of ["video", "audio"]) {
    let e = document.querySelector(`.${section} .items`)
    while (e.firstChild) e.firstChild.remove()
    for (let fn of s.finished[section]) {
      let item = fileItem.cloneNode(true)
      item.querySelector(".filename").textContent = fn
      e.append(item)
    }
  }
}

setInterval(update, 6 * SECOND)
update()

// vi: ts=2 sw=2 et ai
