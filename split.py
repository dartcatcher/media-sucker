#! /usr/bin/python3

import argparse
import json
import statistics
import subprocess
import sys

def timeof(chapternum, param="start_time"):
    for chap in probe["chapters"]:
        if chap["id"] == chapternum:
            return chap[param]


parser = argparse.ArgumentParser(
    description="Splits an MKV file up by chapters",
)
parser.add_argument('filename', help="Input file")
parser.add_argument('-outbase', default="output-p", help="Output file base")
parser.add_argument('-part', default=1, type=int, help="Start number for output parts")
parser.add_argument('-partdigits', default=2, help="Pad output part number with 0 to this width")
parser.add_argument('-autosd', default=2, help="How many standard deviations counts as a short chapter in episode detection")
parser.add_argument('chapters', nargs='*', help="Chapters to include, of the form 'a-b' (eg 1-13)")
args = parser.parse_args()

# Get a list of all chapters
probe_proc = subprocess.run(
    [
        "ffprobe",
        "-print_format", "json",
        "-show_chapters",
        args.filename,
    ],
    capture_output=True,
    encoding="utf-8"
)
probe = json.loads(probe_proc.stdout)

chapters = []
for chap in args.chapters:
    show = [int(v) for v in c.split("-")]
    chapters.append(show)

if not chapters:
    print("No chapters provided, so I'll guess!")
    durations = []
    for chap in probe["chapters"]:
        d = (chap["end"] - chap["start"]) / 1000000000
        chap["duration"] = d
        durations.append(d)
    median = statistics.median(durations)
    shortest = min(durations)
    howshort = statistics.stdev([median, shortest])
    print("- Median chapter duration is {}".format(median))
    print("- Shortest duration: {}".format(shortest))
    print("- Standard deviation of those two: {}".format(howshort))
    if howshort < 1:
        print("Sorry, there is not enough time variance for me to make a good guess.")
        sys.exit(1)

    shorties = []
    for chap in probe["chapters"]:
        sd = statistics.stdev([chap["duration"], shortest])
        if sd < args.autosd:
            shorties.append(chap)
    print("- Number of short tracks: {}".format(len(shorties)))

    if len(shorties) > 6:
        print("Sorry, there are too many short tracks for me to make a good guess.")
        sys.exit(1)

    chapters = []
    for chap in shorties:
        chapters.append(chap["id"])
        chapters.append(chap["id"] + 1)
    if shorties[0]["id"] == 1:
        chapters.append(probe["chapters"][-1]["id"])
    else:
        chapters = [1] + chapters

    chapters = list(zip(chapters[::2], chapters[1::2]))


cmd = [
    "ffmpeg",
    "-i", args.filename,
]
part = args.part
for start, end in chapters:
    st = timeof(start, "start_time")
    et = timeof(end, "end_time")
    chapters = chapters[2:]
    ppart = str(part).rjust(args.partdigits, "0")
    cmd += [
        "-ss", st,
        "-to", et,
        "-map", "0",
        "-c", "copy",
        "{base}{part}.mkv".format(base=args.outbase, part=ppart),
    ]
    print("Part {} (ch {}-{}): {} - {}".format(ppart, start, end, st, et))
    part += 1
subprocess.run(cmd)
