#! /bin/sh

set -e

usage () {
  cat <<EOD
Usage: $0 [-test]

Starts a container to suck media.

-test    Run previously-built container, mounting src, for quick debugging
EOD
}

args="-d --restart=always"
while [ $# -gt 0 ]; do
  case "$1" in
    -test|--test)
      args="--rm -it -v $(realpath $(dirname $0))/src:/app:ro"
      shift
      ;;
    *)
      usage
      exit
      ;;
  esac
done

ip=$(ip --json addr show dev lan | jq -r '.[0].addr_info[0].local')

docker run \
  --name=sucker \
  -p $ip:5801:8080 \
  --device-cgroup-rule 'b 11:* rmw' \
  -v /dev:/hdev \
  -v /srv/ext/incoming/sucker:/incoming/sucker \
  --cpu-shares 256 \
  $args \
  git.woozle.org/neale/media-sucker \
    -incoming /incoming/sucker \
    -drive /hdev/sr0 /hdev/sr1 /hdev/sr2

