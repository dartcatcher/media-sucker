#! /bin/sh

set -e

tag=git.woozle.org/neale/media-sucker
docker build --tag $tag .

case "$1" in
  -push|--push)
    docker push $tag
    ;;
esac

